const prompt = require("prompt-sync")({ sigint: true });

function createDeck() {
    const suits = ["+♠", "+♥", "+♦", "+♣"];
    const ranks = ['ace', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'jack', 'queen', 'king'];
    const deck = []
    for (const s of suits) {
        for (const r of ranks) {
            deck.push(r + s)
        }
    }
    return deck
}

function shuffleDeck(deck) {
    for (let i = 0; i < 52; i++) {
        let tempCard = deck[i];
        let randomIndex = Math.floor(Math.random() * 52)
        deck[i] = deck[randomIndex]
        deck[randomIndex] = tempCard
    }
}

function twoCardHand(Deck) {
    const twoCardHand = Deck.splice(0, 2);
    return twoCardHand
}

function checkScore(input) {
    let valuesFaceOfCards = 0;
    input.map((card) => {
        if (card.includes("♠")) {
            valuesFaceOfCards += 4;
        } else if (card.includes("♥")) {
            valuesFaceOfCards += 3;
        } else if (card.includes("♦")) {
            valuesFaceOfCards += 2
        } else {
            valuesFaceOfCards += 1
        }
    })

    let numberCard = 0
    input.map(card => {
        if (card.includes("king")) {
            numberCard += 10;
        } else if (card.includes("queen")) {
            numberCard += 10;
        } else if (card.includes("jack")) {
            numberCard += 10;
        } else if (card.includes("10")) {
            numberCard += 10;
        } else if (card.includes("ace")) {
            numberCard += 1;
        } else {
            numberCard += Number(card[0])
        }

    })

    const card = [numberCard % 10, valuesFaceOfCards]
    return card

}


function play(bet, banker, Player) {
    let reward = 0;
    if (banker[0] > Player[0]) {
        if ((banker[0] === 8) && ((banker[1] > Player[1]) || (banker[0] > Player[0])) && (banker[1] !== banker[0])) {
            console.log("Banker pok 8 !!!")
            return reward = - bet
        } else if (banker[0] === 8 && ((banker[1] === banker[0]))) {
            console.log("Banker pok 8 2deng!!!")
            return reward = - bet * 2
        } else if (banker[0] === 9 && ((banker[1] > Player[1]) || (banker[0] > Player[0])) && (banker[1] !== banker[0])) {
            console.log("Banker pok 9!!!")
            return reward = - bet
        } else if (banker[0] === 9 && ((banker[1] === banker[0]))) {
            console.log("Banker pok 9 2deng!!!")
            return reward = -bet * 2
        }   else if (backer[0] === backer[1]) {
             console.log("Banker  2 deng!!!")
            return reward = -bet * 2
        } else {
            console.log("Banker Won !!!")
            return reward = - bet
        }
    } else if (Player[0] > banker[0]) {
        if ((Player[0] === 8) && ((Player[1] > banker[1]) || (Player[0] > banker[0])) && (Player[1] !== Player[0])) {
            console.log("Player pok 8 !!!");
            return reward = + bet
        } else if ((Player[0] === 8) && (Player[1] === Player[0])) {
            console.log("Player pok 8 2deng!!!");
            return reward = + bet * 2
        } else if ((Player[0] === 9) && ((Player[1] > banker[1]) || (Player[0] > banker[0])) && (Player[1] !== Player[0])) {
            console.log("Player pok 9!!!");
            return reward = + bet
        } else if ((Player[0] === 9) && ((Player[1] === Player[0]))) {
            console.log("Player pok 9 2 deng!!!");
            return reward = +bet * 2
        } else if ((Player[0] === Player[1])) {
            console.log("Player 2 deng!!!");
            return reward = +bet * 2
        } else {
            console.log('Player won!!!');
            return reward = + bet
        }
    } else {
        if (banker[1] > Player[1]) {
            console.log('banker won!!!');
            return reward = - bet
        } else if (Player[1] > banker[1]) {
            console.log('Player won!!!');
            return reward = + bet
        } else { // ถ้าดอกเท่ากัน
            console.log('draw');
            return reward =  bet
        }
    }
}

function startPokDeng() {
    console.log("Start")
    let start = true
    let yourRewards = 0;
    while (start) {
        let Deck = createDeck().map(card => card.split('+'))
        shuffleDeck(Deck)

        console.log("place a bet")
        let Bet = prompt('');
        let BetNumber = parseInt(Bet);

        let  bankerDraw = twoCardHand(Deck)
        let  playerDraw = twoCardHand(Deck)

        let checkBanker = checkScore(bankerDraw)
        let checkPlayer = checkScore(playerDraw)

        let threeCardHand = [bankerDraw, playerDraw];

        console.log(`Banker`, bankerDraw.map(card => card.join('')))
        console.log(`Player`, playerDraw.map(card => card.join('')))

        for(let i = 0; i < 2; i++){
            console.log('Want to draw?')
            let isDrawExtra = prompt('');
            if (isDrawExtra === "y") {
                threeCardHand[i].push(Deck[0])
                Deck = Deck.filter(card => card !== Deck[0]);
                if (i === 0) {
                    checkBanker = checkScore(threeCardHand[i]);
                    console.log(`Banker`,threeCardHand[i].map(card => card.join('')))
                }
                if (i === 1) {
                    checkPlayer = checkScore(threeCardHand[i]);
                    console.log(`Player`,threeCardHand[i].map(card => card.join('')))
                }
            }
        }

        let  reward = play(BetNumber, checkBanker, checkPlayer)
        yourRewards += reward
        console.log(yourRewards)
        console.log('Start a new game (Y/N)?');
        const playAgain = prompt('');
        if (playAgain === 'y' || playAgain === 'yes' || playAgain === 'YES' || playAgain === 'Y') {
            start = true;
        } else {
            start = false;
            break;
        }
    }
}

startPokDeng()

